import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculo',
  templateUrl: './calculo.component.html',
  styleUrls: ['./calculo.component.css']
})
export class CalculoComponent implements OnInit {

  n1!:any;
  n2!: any;
  total!: number


  constructor() { }

  sumar(){
    this.total=parseInt(this.n1)+parseInt(this.n2);
  }

  ngOnInit(): void {
  }

}
