import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalculoComponent } from './component/calculo/calculo.component';
import { Ejercicio4Component } from './ejercicio4/ejercicio4.component';

@NgModule({
  declarations: [
    AppComponent,
    CalculoComponent,
    Ejercicio4Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
