import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PRODUCTOComponent } from './producto.component';

describe('PRODUCTOComponent', () => {
  let component: PRODUCTOComponent;
  let fixture: ComponentFixture<PRODUCTOComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PRODUCTOComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PRODUCTOComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
