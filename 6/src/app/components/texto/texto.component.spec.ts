import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TEXTOComponent } from './texto.component';

describe('TEXTOComponent', () => {
  let component: TEXTOComponent;
  let fixture: ComponentFixture<TEXTOComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TEXTOComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TEXTOComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
