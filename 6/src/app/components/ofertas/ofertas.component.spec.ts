import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OFERTASComponent } from './ofertas.component';

describe('OFERTASComponent', () => {
  let component: OFERTASComponent;
  let fixture: ComponentFixture<OFERTASComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OFERTASComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OFERTASComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
